<?php
//******************************//
// PhotoAlbum Lang file //
// ---> SP <--- //
//******************************//
define("pa_charse", "windows-1251");
define("pa_lang_code", "sp");
define("pa_date_format", "d.m.Y H:i:s");
// TEXTs
define("pa_txt_loading", "Cargando, espere por favor...<br /><br /><small>( Esta applicacion nercecita javascript enablado)</small>");
define("pa_txt_loading_dir", "Cargando directorio.");
define("pa_txt_no_images_in_dir", "No hay imagen en este directorio.");
define("pa_txt_yes", "Si");
define("pa_txt_no", "No");
define("pa_txt_slideshow", "La exposicion");
define("pa_txt_slideshow_is_disabled", "La exposicion es incapacitado en galleria de foto configuracion.");
define("pa_txt_slideshow_time", "La exposicion demora");
define("pa_txt_slideshow_time_depend_on_conn_speed", "Demora puede ser por combio de conexion velocidad");
define("pa_txt_slideshow_fullscreen", "El major modo es expander la pantalla");
define("pa_txt_slideshow_start", "Comienzo");
define("pa_txt_slideshow_stop", " Alto");
define("pa_txt_imginfo", " IMAGEN");
define("pa_txt_lang", "LENGUAGE");
define("pa_txt_about", "ACERCA DE");
define("pa_txt_fullscreen", "expander la pantalla");
define("pa_txt_date", " fencha");
define("pa_txt_author", "autor");
define("pa_txt_file_size", "tamano");
define("pa_txt_image_resultion", "resolucion");
define("pa_txt_image_link", "Conexion a este imagen");
define("pa_txt_image_exif_camera", "Camara usada");
define("pa_txt_image_exif_exposure_time", "Tiempo de exposicion");
define("pa_txt_image_exif_fnumber", "focal numero");
define("pa_txt_image_exif_focal_length", "focal longitud");
define("pa_txt_image_exif_iso", "ISO velocidad");
define("pa_txt_image_exif_flash", "dstello");
define("pa_txt_homepage", "pra mas informacion visite");
define("pa_txt_close_window", "cerra ventana");
define("pa_txt_poweredby", "Es poderso");
define("pa_txt_select_lang", "Lenguage preferido");

define("pa_error_image_not_supported", "Imagen del tipo no sotenido.");
define("pa_error_image_not_found", "Imagen no encontrado.");
define("pa_txt_mail_my_name", "Su nombre");
define("pa_txt_mail_my_email", "Su email");
define("pa_txt_mail_friend_email", "El email de su amigo");
define("pa_txt_mail_send", "ENVIAR");
define("pa_txt_mail_link_sent", "La conexion se ha mandado...");
define("pa_txt_mail_subject", "Su amigo esta mandando una conexi�n a una imagen...");
define("pa_txt_mail_email_1", "te esta mandando una conexion del album de fotografias.");
define("pa_txt_mail_email_2", "Tu puede encontrar la imagen aqui: ");
?>