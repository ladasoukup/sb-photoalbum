<?php
//******************************//
//     PhotoAlbum Lang file     //
//         ---> EN <---         //
//******************************//
define("pa_charset", "windows-1251");
define("pa_lang_code", "en");
define("pa_date_format", "d.m.Y H:i:s");
// TEXTy
define("pa_txt_loading", "Loading, please wait...<br /><br /><small>(This application needs JavaScript enabled)</small>");
define("pa_txt_loading_dir", "Loading directory.");
define("pa_txt_no_images_in_dir", "There are no images in this drectory.");
define("pa_txt_yes", "yes");
define("pa_txt_no", "no");
define("pa_txt_slideshow", "SLIDESHOW");
define("pa_txt_slideshow_is_disabled", "Slideshow is disabled in photo galery config.");
define("pa_txt_slideshow_time", "Slideshow delay");
define("pa_txt_slideshow_time_depend_on_conn_speed", "delay can be changed due to your connection speed");
define("pa_txt_slideshow_fullscreen", "Use fullscreen mode");
define("pa_txt_slideshow_start", "start");
define("pa_txt_slideshow_stop", "stop");
define("pa_txt_imginfo", "IMAGE");
define("pa_txt_lang", "LANGUAGE");
define("pa_txt_about", "ABOUT");
define("pa_txt_fullscreen", "FULLSCREEN");
define("pa_txt_date", "date");
define("pa_txt_author", "author");
define("pa_txt_file_size", "size");
define("pa_txt_image_resultion", "resultion");
define("pa_txt_image_link", "Link to this image");

define("pa_txt_image_exif_camera", "used camera");
define("pa_txt_image_exif_exposure_time", "exposure time");
define("pa_txt_image_exif_fnumber", "focal number");
define("pa_txt_image_exif_focal_length", "focal length");
define("pa_txt_image_exif_iso", "ISO speed rating");
define("pa_txt_image_exif_flash", "flash");
define("pa_txt_image_exif_exposure_program", "exposure program");
define("pa_txt_image_exif_metering_mode", "metering mode");
define("pa_txt_image_exif_exposure_bias_value", "exposure bias value");

define("pa_txt_homepage", "for more info visit");
define("pa_txt_close_window", "CLOSE WINDOW");
define("pa_txt_poweredby", "Powered by");
define("pa_txt_select_lang", "Select your prefered language");


define("pa_error_image_not_supported", "Image type is not supported.");
define("pa_error_image_not_found", "Image not found.");
define("pa_txt_mail_my_name", "Your name");
define("pa_txt_mail_my_email", "Your email");
define("pa_txt_mail_friend_email", "Friend email");
define("pa_txt_mail_send", "SEND");
define("pa_txt_mail_link_sent", "Link has been sent...");
define("pa_txt_mail_subject", "Your friend is sending you link to image...");
define("pa_txt_mail_email_1", "is sending You link to image from our photo album.");
define("pa_txt_mail_email_2", "You can find image here: ");

?>