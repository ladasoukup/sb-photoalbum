<?php
//******************************//
//     PhotoAlbum Lang file     //
//         ---> SE <---         //
//     Swedish translation by   //
//         Jan Johansson        //
//******************************//
define("pa_charset", "ISO-8859-1");
define("pa_lang_code", "se");
define("pa_date_format", "Y-m-d H:i:s");
// TEXTy
define("pa_txt_loading", "Progammet initieras...<br /><br /><small>(Detta program anv�nder JavaScript)</small>");
define("pa_txt_loading_dir", "Laddar mapp.");
define("pa_txt_no_images_in_dir", "Det finns inga bilder i denna mappen.");
define("pa_txt_yes", "Ja");
define("pa_txt_no", "Nej");
define("pa_txt_slideshow", "BILDSPEL");
define("pa_txt_slideshow_is_disabled", "Bildspel �r avst�ngd i denna konfigurationen.");
define("pa_txt_slideshow_time", "Tid mellan bildv�xling");
define("pa_txt_slideshow_time_depend_on_conn_speed", "Minimum-tiden beror p� din anslutningshastighet");
define("pa_txt_slideshow_fullscreen", "Anv�nd fullsk�rm");
define("pa_txt_slideshow_start", "Start");
define("pa_txt_slideshow_stop", "Stopp");
define("pa_txt_imginfo", "BILDINFO");
define("pa_txt_lang", "LANGUAGE");
define("pa_txt_about", "PROGRAMINFO");
define("pa_txt_fullscreen", "FULLSK�RM");
define("pa_txt_date", "Datum");
define("pa_txt_author", "Kreat�r");
define("pa_txt_file_size", "Storlek");
define("pa_txt_image_resultion", "Uppl�sning");
define("pa_txt_image_link", "L�nk till denna bilden");
define("pa_txt_image_exif_camera", "Anv�nd kamera");
define("pa_txt_image_exif_exposure_time", "Exponeringstid");
define("pa_txt_image_exif_fnumber", "Bl�ndare");
define("pa_txt_image_exif_focal_length", "Sk�rpeavst�nd");
define("pa_txt_image_exif_iso", "ISO filmhastighet");
define("pa_txt_image_exif_flash", "Blixt");
define("pa_txt_homepage", "F�r mer information");
define("pa_txt_close_window", "St�ng f�nstret");
define("pa_txt_poweredby", "Platform");
define("pa_txt_select_lang", "V�lj �nskat spr�k");

define("pa_error_image_not_supported", "Ingen support f�r bildformatet."); 
define("pa_error_image_not_found", "Hittar inte bilden."); 
define("pa_txt_mail_my_name", "Ditt Namn"); 
define("pa_txt_mail_my_email", "Din email"); 
define("pa_txt_mail_friend_email", "Kompisens email"); 
define("pa_txt_mail_send", "S�ND"); 
define("pa_txt_mail_link_sent", "L�nken �r skickad..."); 
define("pa_txt_mail_subject", "Rekommenderad l�nk till en bild, fr�n en kompis..."); 
define("pa_txt_mail_email_1", "vill visa dig en bild i v�rt fotogalleri."); 
define("pa_txt_mail_email_2", "Du kan se bilden h�r: "); 

?>