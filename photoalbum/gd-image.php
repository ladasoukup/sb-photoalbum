<?php
/*
******************************************************************************************
** SB|photoAlbum                                                                        **
** Copyright (C)2005 Ladislav Soukup                                                    **
**                                                                                      **
** URL: http://php.soundboss.cz                                                         **
** URL: http://www.soundboss.cz                                                         **
******************************************************************************************
*/
error_reporting(E_ERROR);
if (strstr($_GET["dir"], "..")) die();
if (strstr($_GET["img"], "..")) die();
chdir("..");
include_once "./pa_config.php";
include_once "./photoalbum/core.php";
$pa_core = new pa_core();
$img_path = pa_image_dir . $_GET["dir"] . "/" . $_GET["img"];
// OUTPUT
Header("Content-type: image/jpeg");
$exp=GMDate("D, d M Y H:i:s",time()+999);
Header("Expires: $exp GMT");
if (!pa_image_add_logo) {
	// READ IMAGE FROM FILE
	$fp = fopen($img_path, "rb");
	while(!feof($fp)) {
		$buf = fread($fp, 4096);
		echo $buf;
		$bytesSent+=strlen($buf);    /* We know how many bytes were sent to the user */
	}
} else {
	// GENERATE IMAGE...
	
	if (file_exists($img_path)){
		$info=getimagesize($img_path);
		$width=$info[0];
		$height=$info[1];
		if ($info[2]==1){
			$img=@imagecreatefromgif($img_path);
		} else if ($info[2]==2){
			$img=@imagecreatefromjpeg($img_path);
		} else if ($info[2]==3){
			$img=@imagecreatefrompng($img_path);
		} else {
			$width=500;
			$height=40;
			$img = $pa_core->NewImage($width, $height, pa_error_image_not_supported);
		}
	} else {
		$width=500;
		$height=40;
		$img = $pa_core->NewImage($width, $height, pa_error_image_not_found);
	}
	
	if (file_exists(pa_image_logo)){
		$info=getimagesize(pa_image_logo);
		$logo_width=$info[0];
		$logo_height=$info[1];
		if ($info[2]==1){
			$img_logo=imagecreatefromgif(pa_image_logo);
		} else if ($info[2]==2){
			$img_logo=imagecreatefromjpeg(pa_image_logo);
		} else if ($info[2]==3){
			$img_logo=imagecreatefrompng(pa_image_logo);
		} else {
			$logo_width=300;
			$logo_height=40;
			$img_logo = $pa_core->NewImage($logo_width, $logo_height, pa_error_image_not_supported);
		}
		// positioning - X
		if (pa_image_logo_x=="left"){
			$dst_x=10;
		} else if (pa_image_logo_x=="center"){
			$dst_x=round(($width-$logo_width)/2);
		} else if (pa_image_logo_x=="right"){
			$dst_x=$width-10-$logo_width;
		} else {
			$dst_x=round(($width-$logo_width)/2);
		}
		// positioning - Y
		if (pa_image_logo_y=="top"){
			$dst_y=5;
		} else if (pa_image_logo_y=="middle"){
			$dst_y=round(($height-($logo_height/2))/2);
		} else if (pa_image_logo_y=="bottom"){
			$dst_y=$height-5-$logo_height;
		} else {
			$dst_y=round(($height-($logo_height/2))/2);
		}
		imagecopy($img, $img_logo, $dst_x, $dst_y, 0, 0, $logo_width, $logo_height);
	}
	ImageJpeg($img, "", pa_image_quality);
}
?>