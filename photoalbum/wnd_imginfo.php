<?php
/*
******************************************************************************************
** SB|photoAlbum                                                                        **
** Copyright (C)2005 Ladislav Soukup                                                    **
**                                                                                      **
** URL: http://php.soundboss.cz                                                         **
** URL: http://www.soundboss.cz                                                         **
******************************************************************************************
*/
error_reporting(E_ERROR);
if (strstr($_GET["dir"], "..")) die();
if (strstr($_GET["img"], "..")) die();
chdir("..");
include_once "./pa_config.php";
define("pa_header_include_subdir", false);
include_once "./photoalbum/html_header.php";
include_once "./photoalbum/core.php";
$pa_core = new pa_core();
$img_info = $pa_core->parseImgInfo($_GET["dir"], $_GET["img"]);
$img_exif = $pa_core->parseImgExif($_GET["dir"], $_GET["img"]);
//print_r($img_exif);
$image_url = $pa_core->URLStripLastDir(pa_home_url) . "pa_index.php?folder=" . $_GET["dir"] . "&amp;image=" . $_GET["img"];
if ((!empty($img_exif["SubIFD"]["DateTimeOriginal"])) && (trim($img_exif["SubIFD"]["DateTimeOriginal"]) != "0000:00:00 00:00:00")){
	$image_date_temp = explode(" ", trim($img_exif["SubIFD"]["DateTimeOriginal"]));
	$image_date_temp_date = explode(":", $image_date_temp[0]);
	$image_date_temp_time = explode(":", $image_date_temp[1]);
	$image_date = mktime($image_date_temp_time[0], $image_date_temp_time[1], $image_date_temp_time[2], $image_date_temp_date[1], $image_date_temp_date[2], $image_date_temp_date[0]);
	//int mktime ( [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )
	$image_date = date(pa_date_format, $image_date);
	//$image_date = $img_exif["SubIFD"]["DateTimeOriginal"];
} else {
	$image_date = $img_info["date"];
}
?>
<body>
<h1 id="img_name"><?php echo $_GET["img"]; ?></h1>
<div class="hline">&nbsp;</div>
<div style="padding-top: 5px;"><?php echo $img_info["text"]; ?></div>
<div style="padding-top: 10px;">
<?php if (!empty($img_info["author"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_author; ?>:</span> <?php echo $img_info["author"]; ?><br /><?php } ?>
<span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_date; ?>:</span> <?php echo $image_date; ?><br />
<span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_file_size; ?>:</span> <?php echo $img_info["filesize"]; ?><br />
<span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_resultion; ?>:</span> <?php echo $img_info["imageresultion"]; ?><br />
<?php if (!empty($img_exif["IFD0"]["Model"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_camera; ?>:</span> <?php echo $img_exif["IFD0"]["Make"] . " " . $img_exif["IFD0"]["Model"]; ?><br /><?php } ?>
<?php if (!empty($img_exif["SubIFD"]["ExposureTime"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_exposure_time; ?>:</span> <?php echo $img_exif["SubIFD"]["ExposureTime"]; ?><br /><?php } ?>
<?php if (!empty($img_exif["SubIFD"]["FNumber"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_fnumber; ?>:</span> <?php echo $img_exif["SubIFD"]["FNumber"]; ?><br /><?php } ?>
<?php if (!empty($img_exif["SubIFD"]["ExposureBiasValue"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_exposure_bias_value; ?>:</span> <?php echo $img_exif["SubIFD"]["ExposureBiasValue"]; ?><br /><?php } ?>
<?php if (!empty($img_exif["SubIFD"]["FocalLength"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_focal_length; ?>:</span> <?php echo $img_exif["SubIFD"]["FocalLength"]; ?><br /><?php } ?>
<?php if (!empty($img_exif["SubIFD"]["ISOSpeedRatings"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_iso; ?>:</span> <?php echo $img_exif["SubIFD"]["ISOSpeedRatings"]; ?><br /><?php } ?>
<?php if (!empty($img_exif["SubIFD"]["ExposureProgram"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_exposure_program; ?>:</span> <?php echo $img_exif["SubIFD"]["ExposureProgram"]; ?><br /><?php } ?>
<?php if (!empty($img_exif["SubIFD"]["MeteringMode"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_metering_mode; ?>:</span> <?php echo $img_exif["SubIFD"]["MeteringMode"]; ?><br /><?php } ?>
<?php if (!empty($img_exif["SubIFD"]["Flash"])) { ?><span style="font-weight: bold; text-transform: capitalize;"><?php echo pa_txt_image_exif_flash; ?>:</span> <?php echo $img_exif["SubIFD"]["Flash"]; ?><br /><?php } ?>
</div>
<br /><div class="hline">&nbsp;</div>
<div style="font-weight: bold;"><?php echo pa_txt_image_link; ?>:</div>
<div style="font-size: 9px; margin: 5px;"><?php echo $image_url; ?></div>
<div style="padding-top: 10px; text-align: center;"><div class="toolbar_btn" onclick="self.close()"><?php echo pa_txt_close_window; ?></div></div>
<script language="JavaScript" type="text/javascript">
if (document.getElementById('img_name').innerHTML == "undefined"){
	self.close();
}
</script>
</body>
</html>