<?php
/*
******************************************************************************************
** SB|photoAlbum                                                                        **
** Copyright (C)2005 Ladislav Soukup                                                    **
**                                                                                      **
** URL: http://php.soundboss.cz                                                         **
** URL: http://www.soundboss.cz                                                         **
******************************************************************************************
*/
error_reporting(E_ERROR);
if (strstr($_GET["dir"], "..")) die();
if (strstr($_GET["img"], "..")) die();
chdir("..");
include_once "./pa_config.php";
define("pa_header_include_subdir", false);
include_once "./photoalbum/html_header.php";
include_once "./photoalbum/core.php";
include_once "./photoalbum/class_mail/htmlMimeMail5.php";
$pa_core = new pa_core();
$image_url = $pa_core->URLStripLastDir(pa_home_url) . "pa_index.php?folder=" . $_GET["dir"] . "&amp;image=" . $_GET["img"];
$img_url = $pa_core->URLStripLastDir(pa_home_url) . "pa_index.php?folder=" . rawurlencode($_GET["dir"]) . "&amp;image=" . rawurlencode($_GET["img"]);
$img_path = pa_home_url . "gd-image.php?dir=".rawurlencode($_GET["dir"])."&img=".rawurlencode($_GET["img"]);
?>
<body>
<?php
if ( (empty($_POST["my_name"])) || (empty($_POST["my_email"])) || (empty($_POST["friend_email"])) ) {
	// Show form
?>
<h1 id="img_name"><?php echo $_GET["img"]; ?></h1>
<div class="hline">&nbsp;</div>
<div style="padding-top: 5px;"><?php echo $img_info["text"]; ?></div>
<div style="padding-top: 10px;">

<form name="mail_from" id="mail_from" action="wnd_maillink.php?dir=<?php echo $_GET["dir"]; ?>&amp;img=<?php echo $_GET["img"]; ?>" method="post">
<label for="my_name" class="frm"><?php echo pa_txt_mail_my_name; ?>:</label><input name="my_name" value="" class="frm" /><br />
<label for="my_email" class="frm"><?php echo pa_txt_mail_my_email; ?>:</label><input name="my_email" value="" class="frm" /><br />
<label for="friend_email" class="frm"><?php echo pa_txt_mail_friend_email; ?>:</label><input name="friend_email" value="" class="frm" /><br />
<div style="clear: both;">&nbsp;</div>
</form>
<div style="padding-top: 0px; text-align: center;"><div class="toolbar_btn" onclick="mail_from.submit()"><?php echo pa_txt_mail_send; ?></div></div>

</div>
<br /><div class="hline">&nbsp;</div>
<div style="font-weight: bold;"><?php echo pa_txt_image_link; ?>:</div>
<div style="font-size: 9px; margin: 5px;"><?php echo $image_url; ?></div>
<div style="padding-top: 10px; text-align: center;"><div class="toolbar_btn" onclick="self.close()"><?php echo pa_txt_close_window; ?></div></div>
<script language="JavaScript" type="text/javascript">
if (document.getElementById('img_name').innerHTML == "undefined"){
	self.close();
}
</script>
<?php
} else {
	// Mail link

		ob_start();
		include "./photoalbum/wnd_maillink_template.php";
		$html_text = ob_get_contents();
		ob_end_clean();
		$mail=new htmlMimeMail5();
		@ini_set("sendmail_from", $_POST["my_email"]);
		$mail->SetFrom($_POST["my_email"]);
		$mail->SetSubject(iconv(pa_charset, "UTF-8", pa_txt_mail_subject));
		$mail->setHTML($html_text);
		$mail->SetHeader("X-Mailer","SBphotoAlbum via PHP/" . phpversion());

		if ($mail->send(array($_POST["friend_email"]), "mail")){
			$return="OK";
		} else {
			$return="error";
		}
		unset($mail); 

?>
<h1><?php echo pa_txt_mail_link_sent; ?></h1>
<div style="padding-top: 10px; text-align: center;"><div class="toolbar_btn" onclick="self.close()"><?php echo pa_txt_close_window; ?></div></div>
<?php } ?>
</body>
</html>

