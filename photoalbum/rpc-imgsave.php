<?php
/*
******************************************************************************************
** SB|photoAlbum                                                                        **
** Copyright (C)2005 Ladislav Soukup                                                    **
**                                                                                      **
** URL: http://php.soundboss.cz                                                         **
** URL: http://www.soundboss.cz                                                         **
******************************************************************************************
*/
error_reporting(E_ERROR);
if (strstr($_GET["dir"], "..")) die();
if (strstr($_GET["img"], "..")) die();
chdir("..");
include_once "./pa_config.php";
define("pa_header_include_subdir", false);
include_once "./photoalbum/core.php";
$pa_core = new pa_core();
$img_info = $pa_core->parseImgInfo($_GET["dir"], $_GET["img"]);
$img_path = pa_image_dir . $_GET["dir"] . "/" . $_GET["img"];

$file_extension = strtolower(substr(strrchr($_GET["img"],"."),1));
switch ($file_extension) {
	case "gif": $ctype="image/gif"; break;
	case "png": $ctype="image/png"; break;
	case "jpe": case "jpeg":
	case "jpg": $ctype="image/jpg"; break;
	default: $ctype="application/force-download";
}
if (pa_image_add_logo) $ctype="image/jpg";
header("Content-Type: " . $ctype);
header("Content-Disposition: attachment; filename=\"" . $_GET["img"] . "\"");
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: private");
if (!pa_image_add_logo) {
	header("Content-Length: ".filesize($img_path));
	$pa_core->readfile_chunked($img_path);
} else {
	$img_path = pa_home_url . "gd-image.php?dir=".rawurlencode($_GET["dir"])."&img=".rawurlencode($_GET["img"]);
	//echo $img_path;
	$pa_core->readfile_chunked($img_path);
}
?>