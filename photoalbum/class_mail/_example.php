<?php
if (APP_TOKEN != md5($_SERVER["SERVER_SIGNATURE"])) die("ACCESS DENIED");
if (!empty($cron["cron_params"])) {
	$send_max=$cron["cron_params"];
} else {
	$send_max=10;
}
$max_errors=5;
include_once $cron_mod_path."htmlMimeMail5.php";

// Find all dead emails
$sql=$db->get_results("SELECT * FROM ".DB_PREFIX."queue_mail WHERE error_count>'".$max_errors."'",ARRAY_A);
if (!empty($sql)){
	foreach($sql as $data){
		$body["from"]=$data["mail_from"];
		$body["to"]=$data["mail_to"];
		$mail_body=array("template"=>"mail_queue_error.tpl","subject"=>"EMAIL DELIVERY PROBLEM","body"=>$body);
		$core->QueueMail(array("from"=>AdminEmail,"to"=>ErrorReportEmail),$mail_body,NOW_DT,90);
		$db->query("DELETE FROM ".DB_PREFIX."queue_mail WHERE id='".$data["id"]."'");
		$log_detail.="Nedorucitelny email: ".$data["mail_from"]." -- ".$data["mail_to"]."\n";
	}
}
// SENDING EMAILs
$sql=$db->get_results("SELECT * FROM ".DB_PREFIX."queue_mail WHERE mail_date<'".NOW_DT."' ORDER BY mail_date ASC, mail_priority ASC LIMIT ".$send_max."",ARRAY_A);
if (!empty($sql)){
	foreach($sql as $data){
		@ini_set("sendmail_from",$data["mail_from"]);
		$mail=new htmlMimeMail5();
		//$mail->setReturnPath($data["mail_from"]);
		$mail->SetFrom($data["mail_from"]);
		$mail->SetSubject($data["mail_subject"]);
		$mail->setHTML($data["mail_text"],"./");
		if ($data["mail_priority"]<25) $mail->SetHeader("X-Priority","1 (Highest)");
		if ($data["mail_priority"]>75) $mail->SetHeader("X-Priority","5 (Lowest)");
		$mail->SetHeader("X-Mailer","SBCMS 2005 QUEUE MAILER via PHP/" . phpversion());
		// Attachments
		if (!empty($data["mail_files"])){
			$files=explode("\n",$data["mail_files"]);
			foreach($files as $files_name){
				$files_name=trim($files_name);
				$mail->addAttachment(new fileAttachment($files_name));
			}
		}
		$log_detail.="Sending msg. id: ".$data["id"]." to ".$data["mail_to"]."...";
		if ($mail->send(array($data["mail_to"]), "mail")){
			$return="OK";
			$log_detail.="OK\n";
			$db->query("DELETE FROM ".DB_PREFIX."queue_mail WHERE id='".$data["id"]."'");
		} else {
			$return="error";
			$log_detail.="Failed!\n";
			$date=date("Y-m-d H:i:s",time()+(60*10)-10);
			$db->query("UPDATE ".DB_PREFIX."queue_mail SET mail_date='".$date."', error_count='".($data["error_count"]+1)."' WHERE id='".$data["id"]."'");
		}
		unset($mail);
	}
}
$out.=$log_detail;
?>